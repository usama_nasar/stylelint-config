"use strict";

const kebabCase = /^([a-z][a-z0-9]*)(-[a-z0-9]+)*$/;

module.exports = {
    plugins: [
        "stylelint-scss"
    ],
    // IMPORTANT: Please keep these rules in alphabetical order.
    rules: {
        "at-rule-blacklist": ["debug", "warn"],
        "at-rule-empty-line-before": [
            "always",
            {
                except: ["blockless-after-same-name-blockless", "first-nested"],
                ignore: ["after-comment"]
            }
        ],
        "at-rule-name-case": "lower",
        "at-rule-name-space-after": "always-single-line",
        "at-rule-no-vendor-prefix": true,
        "at-rule-semicolon-newline-after": "always",
        "at-rule-semicolon-space-before": "never",
        "block-closing-brace-empty-line-before": "never",
        "block-closing-brace-newline-after": "always",
        "block-closing-brace-newline-before": "always-multi-line",
        "block-closing-brace-space-before": "always-single-line",
        "block-no-empty": true,
        "block-opening-brace-newline-after": "always-multi-line",
        "block-opening-brace-space-after": "always-single-line",
        "block-opening-brace-space-before": "always",
        "color-hex-case": "lower",
        "color-hex-length": "short",
        "color-named": "never",
        "color-no-invalid-hex": true,
        "comment-empty-line-before": [
            "always",
            {
                except: ["first-nested"],
                ignore: ["stylelint-commands"]
            }
        ],
        "comment-no-empty": true,
        "comment-whitespace-inside": "always",
        "custom-media-pattern": kebabCase,
        "custom-property-empty-line-before": [
            "always",
            {
                except: [
                    "after-custom-property",
                    "first-nested"
                ],
                ignore: [
                    "after-comment",
                    "inside-single-line-block"
                ]
            }
        ],
        "custom-property-pattern": kebabCase,
        "declaration-bang-space-after": "never",
        "declaration-bang-space-before": "always",
        "declaration-block-no-duplicate-properties": true,
        "declaration-block-no-redundant-longhand-properties": true,
        "declaration-block-no-shorthand-property-overrides": true,
        "declaration-block-semicolon-newline-after": "always-multi-line",
        "declaration-block-semicolon-space-after": "always-single-line",
        "declaration-block-semicolon-space-before": "never",
        "declaration-block-single-line-max-declarations": 1,
        "declaration-block-trailing-semicolon": "always",
        "declaration-colon-newline-after": "always-multi-line",
        "declaration-colon-space-after": "always-single-line",
        "declaration-colon-space-before": "never",
        "declaration-empty-line-before": [
            "always",
            {
                except: [
                    "after-declaration",
                    "first-nested"
                ],
                ignore: [
                    "after-comment",
                    "inside-single-line-block"
                ]
            }
        ],
        "declaration-no-important": true,
        "declaration-property-unit-whitelist": {
            "/^font(-size)?$/": ["em", "rem"],
            // See https://developer.mozilla.org/en-US/docs/Web/CSS/line-height#Prefer_unitless_numbers_for_line-height_values.
            "line-height": []
        },
        "declaration-property-value-blacklist": [
            {
                // See https://stackoverflow.com/a/8962837/221528.
                "/^transition(-property)?$/": ["/\\ball\\b/"]
            },
            {
                // TODO: See https://github.com/stylelint/stylelint/issues/4117.
                message: "Avoid using 'transition: all', for performance reasons."
            }
        ],

        "scss/at-rule-no-unknown": true
    }
};
