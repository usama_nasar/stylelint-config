# StyleLint Config

A shareable [Stylelint](https://stylelint.io/) config for projects.

## Installation

Run `npm i -D icode-stylelint-config` to install this package.

Also run following command to install its required dependencies

`npm i -D stylelint`

### Make compatible with prettier

Run `npm i -D stylelint-config-prettier` to install this package.

### StyleLint for scss

Run `npm i -D stylelint-scss` to install this package.

### Configure Prettier to run on commit

Run `npm i -D husky lint-staged` to install this package.

After running this command, add this to package.json to enable eslint on commit

    "scripts": {
        "lint:styles": "stylelint \"src/**/*.{css,scss}\""
    },
    "husky": {
    	"hooks": {
    		"pre-commit": "lint-staged"
    	}
    },
    "lint-staged": {
        "*.{css,scss}": [
            "prettier --write",
            "stylelint --fix --syntax scss",
            "git add"
        ]
    },
    "stylelint": {
        "extends": [
            "icode-stylelint-config"
        ]
    }
